
<?php
//Archivo php donde se gestiona el logueo un usuario, Autor: yn8idg, Fecha: 11/11/2017
session_start();

//Comprueba que si es un POST proveniente de Form de Login
if(!isset($_REQUEST['login']) && !(isset($_REQUEST['password']))){
		
	//Devuelve el el formulario para loguearse
		include '../Views/LOGIN.php';
		$login = new Login();
		
}
else{

	
	include '../Models/USUARIOS_Model.php';
	$usuario = new USUARIOS_Model($_REQUEST['login'],'',$_REQUEST['password'],'','','','','','','');
	$respuesta = $usuario->login();

	//Comprueba si el usuario existe y coincide con la contraseña
	if ($respuesta == 'true'){
		session_start();
		
		$_SESSION['login'] = $_REQUEST['login'];
		header('Location: ../index.php');
	}
	else{
		//Si hay algun fallo devuelve el error devuelto por el modelo de datos
		include '../Views/MESSAGE.php';
		new MESSAGE($respuesta, '../Controllers/Login_Controller.php');
	}

}

?>
