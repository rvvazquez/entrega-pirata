
<?php
//Archivo php donde se gestiona las acciones principales un usuario logueado, Autor: yn8idg, Fecha: 11/11/2017
session_start();
include '../Functions/Authentication.php';
include '../Views/MESSAGE.php';





if (!IsAuthenticated()){

	new MESSAGE('You need sign in', '../Controllers/Login_Controller.php');

}else{

	

include '../Models/USUARIOS_Model.php';
include '../Views/Usuario_SHOWALL.php';
include '../Views/Usuario_ADD.php';
include '../Views/Usuario_SEARCH.php';
include '../Views/Usuario_SHOWCURRENT.php';
include '../Views/Usuario_EDIT.php';
include '../Views/Usuario_DELETE.php';





function get_data_form(){

	$login = $_REQUEST['login'];
	$DNI = $_REQUEST['DNI'];
	$password = $_REQUEST['password'];
	$emailuser = $_REQUEST['email'];
	$fechnacuser = $_REQUEST['FechaNacimiento'];
	$telefono = $_REQUEST['telefono'];
	$nombreuser = $_REQUEST['nombre'];
	$apellidosuser = $_REQUEST['apellidos'];
	
	$sexo = $_REQUEST['sexo'];
	$action = $_REQUEST['action'];

	if(isset($_FILES['fotopersonal'])){
		$foto = $_FILES['fotopersonal'];
	}else{
		$foto = $_REQUEST['fotopersonal'];
	}

	
	

	$USUARIOS = new USUARIOS_Model(
		$login, 
		$DNI,
		$password, 
		$emailuser, 
		$fechnacuser, 
		$nombreuser, 
		$apellidosuser, 
		$telefono, 
		$foto, 
		$sexo);

	return $USUARIOS;
	}

	if (!isset($_REQUEST['action'])){
		$_REQUEST['action'] = '';
	}


	Switch ($_REQUEST['action']){
		case 'ADD':
			if (!$_POST){
				new USUARIOS_ADD();
			}
			else{
				$USUARIOS = get_data_form();
				$respuesta = $USUARIOS->ADD();
				new MESSAGE($respuesta, '../Controllers/USUARIOS_Controller.php');
			}
			break;
		case 'DELETE':
			if (!$_POST){
				$USUARIOS = new USUARIOS_Model($_REQUEST['login'], '', '', '', '', '', '', '', '', '');
				$valores = $USUARIOS->RellenaDatos();
				new USUARIOS_DELETE($valores);
			}
			else{
				$USUARIOS = new USUARIOS_Model($_REQUEST['login'], '', '', '', '', '', '', '', '', '');
				$respuesta = $USUARIOS->DELETE();
				if(($_SESSION['login'] == $_REQUEST['login']) && $respuesta == 'Correctly delete')
					unset($_SESSION['login']);  
				
				new MESSAGE($respuesta, '../Controllers/USUARIOS_Controller.php');
			}
			break;
		case 'EDIT':		
			
			if (!$_POST){
				
				$USUARIOS = new USUARIOS_Model($_REQUEST['login'], '', '', '', '', '', '', '', '', '');
				$valores = $USUARIOS->RellenaDatos();
				new USUARIOS_EDIT($valores);
			}
			else{
				
				$USUARIOS = get_data_form();

				$respuesta = $USUARIOS->EDIT();
				new MESSAGE($respuesta, '../Controllers/USUARIOS_Controller.php');
			}
			
			break;
		case 'SEARCH':
			if (!$_POST){
				new USUARIOS_SEARCH();
			}
			else{
				$USUARIOS = get_data_form();
				$datos = $USUARIOS->SEARCH();
				$lista = array('login','password','fechnacuser','grupopracticasuser','emailuser','nombreuser','apellidosuser','cursoacademicouser','titulacionuser');
				new USUARIOS_SHOWALL($lista, $datos, '../Controllers/USUARIOS_Controller.php');
			}
			break;
		case 'SHOWCURRENT':
			$USUARIOS = new USUARIOS_Model($_REQUEST['login'],'', '', '', '', '', '', '', '', '');
			$valores = $USUARIOS->RellenaDatos();
			new USUARIOS_SHOWCURRENT($valores);
			break;
		default:
			
			if (!$_POST){
				$USUARIOS = new USUARIOS_Model('','','', '', '', '', '', '', '', '');
			}
			else{
				$USUARIOS = new USUARIOS_Model($_REQUEST['login'], '', '', '', '', '', '', '', '', '');
			}
			$datos = $USUARIOS->AllData();
			$lista = array('login','password','fechnacuser','grupopracticasuser','emailuser','nombreuser','apellidosuser','cursoacademicouser','titulacionuser');
			new USUARIOS_SHOWALL($lista, $datos, '../Controllers/USUARIOS_Controller.php');
	

	}
						
}
?>