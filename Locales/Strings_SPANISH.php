<?php 
//Archivo php donde se guarda strings en español, Autor: yn8idg, Fecha: 11/11/2017
$strings = 
array(
	'Portal de Gestión' => 'Management website',
	'Usuario no autenticado' => 'User not logged',
	'Login' => 'Usuario', 
	'Password' => 'Contraseña',
	'Language' => 'Language',
	'User' => 'User',
	'ENTREGAS' => 'Deliverables',
	'QA' => 'QA',
	'ENGLISH' => 'ENGLISH',
	'SPANISH' => 'SPANISH',
	'The login dont exists' => 'El login no existe',
	'Back' => 'Volver',
	'Incorrect password for this login' => 'Contraseña incorrecta para login',
	'Register' => 'Registrar',
	'Add error' => 'Error de añadido',
	'User Interface Management' => 'User Interface Management',
	'Phone' => 'Telefono',
	'Name' => 'Nombre',
	'Surname' => 'Apellido',
	'Sex' => 'Sexo',
	'Photo' => 'Foto',
	'Date birth' => 'Fecha Nacimiento',
	'DNI' => 'DNI',
	'¿Have not you account? Sign up' => '¿No tienes cuenta? Registrate',
	'Add' => 'Añadir',
	'Sign up' => 'Registrarse',
	'Email' => 'Correo Electronico',
	'hombre' => 'Hombre',
	'mujer' => 'Mujer',
	'Delete' => 'Borrar',
	'Edit' => 'Editar',
	'Search' => 'Buscar',
	'ShowCurrent' => 'Detallado',
	'No exist image' => 'No existe imagen',
	'You need sign in' => 'Necesitas registrarte',
	'It is not possible connect to DB' => 'No es posible conectar con la base de datos',
	'Duplicate email' => 'Ya existe una cuenta con ese email',
	'Duplicate DNI' => 'Ya existe una cuenta con ese DNI',
	'Unknowed Error' => 'Error desconocido',
	'Success insert' => 'Insercion correcta',
	'It is already in DB' => 'Ya existe en la base de datos',
	'Introduce a value' => 'Introduce un valor',
	'It does not exist in DB' => 'No existe en la base de datos',
	'Query Error about DB' => 'Error de la consulta en la base de datos',
	"Correctly delete" =>  "Correctamente borrado",
	"It does not exist" => "No existe",
	'Success Modify' => 'Modificado correctamente',
	'User does not exists' => 'El usuario no existe',
	'Password for this user does not exists' => 'La contraseña para este usuario no existe',
	'User already exists' => 'El usuario ya existe',
	'Your actual photo' => 'Tu foto actual',
	'Error image insert' => 'Error de insercion de imagen',
	'El campo' => 'El campo',
	'is empty' => 'esta vacio',
	'Introduce less of' => 'Introduce menos de',
	'characters for' => 'caracteres para',
	'Introduced characters not allow' => 'Caracter incorrecto no permitido',
	'characters' => 'caracter',
	'Introduce numerical value' => 'Introduce valor numerico',
	'Introduce a value between' => 'Introduce un valor entre',
	'and' => 'y',
	'Do not introduce letters' => 'No introduzcas letras',
	'Incorrect format for DNI' => 'Format de DNI incorrecto',
	'Incorrect length for DNI' => 'Longitud invalida para DNI',
	'Incorrect format number' => 'Formato de numero incorrecto',
	'Incorrect format email' => 'Formato de email incorrecto',
	'Can not content whitespaces' => 'No puede contener espacios en blanco',
	'Can not content whitespaces at beginning' => 'No puede contener espacios en blanco al principio',
	'NewPassword' => 'Nueva Contraseña',
	'Date' => 'Fecha',
	'Author' => 'Autor',
	'Option' => 'Opcion',
	'Suboption' => 'Subopcion'
)
;
 ?>
