<?php
//Archivo php donde arranca la aplicacion web, Autor: yn8idg, Fecha: 11/11/2017
//entrada a la aplicacion

//se va usar la session de la conexion
session_start();

//funcion de autenticacion
include './Functions/Authentication.php';

//si no ha pasado por el login de forma correcta
if (!IsAuthenticated()){
	header('Location:./Controllers/Login_Controller.php');
}
//si ha pasado por el login de forma correcta 
else{
	header('Location:./Controllers/USUARIOS_Controller.php');
}

?>
