<?php
    //Archivo php donde se muestra el apartado Mostrar todos los usuarios: yn8idg, Fecha: 11/11/2017
    include_once '../Functions/Authentication.php';
    
    
    class USUARIOS_SHOWALL {

        function __construct($lista,$users,$message){
            
            $this->pinta($users,$message);

        }

    

    function pinta($users,$message){

        if(!isset($_SESSION['idioma'])){
            $_SESSION['idioma'] = 'SPANISH'; 
        }

        include_once '../Locales/Strings_index.php';

        foreach($stringslang as $lang){
            if($lang == $_SESSION['idioma'])
                include_once '../Locales/Strings_'. $lang .'.php';
        }

        include '../Views/HEADER_View.php';

        new HEADER();
        ?>
                    <aside id="menu">
                        <div class="resp-menu-close" onclick="hide()">
                            <img src="../Views/imgs/cross.png" alt="">
                        </div>
                        <ul>
                            <li onclick="show()" tabindex="1">
                            <?php echo $strings['Option']; ?> 1
                                <ul class="sub" id="pri">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 2
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 3
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 4
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 5
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                        </ul>
                    </aside>
            <section class="form-basic-start"> 

            <div class="showall">
    
                <div >
                    <form  action="<?php echo $message; ?>" method="">
                        <button class="showall-action" name="action" value="SEARCH" type="submit"><img src="../Views/imgs/search.png" alt="" srcset=""></button>
                        <button class="showall-action" name="action" value="ADD" type="submit"><img src="../Views/imgs/add.png" alt="" srcset=""></button>
                    </form>
                        
                </div>
                
                <table class="showall-tab">
                    <tr>
                        <th><?php echo $strings['Login']; ?></th>
                        <th><?php echo $strings['Password']; ?></th> 
                        <th><?php echo $strings['DNI']; ?></th>
                        <th><?php echo $strings['Date birth']; ?></th>
                        <th><?php echo $strings['Phone']; ?></th>
                        <th><?php echo $strings['Email']; ?></th>
                        <th><?php echo $strings['Name']; ?></th>
                        <th><?php echo $strings['Surname']; ?></th>
                        <th><?php echo $strings['Photo']; ?></th>
                        <th><?php echo $strings['Sex']; ?></th>
                        <th>
                        </th>
                    </tr>
                    <?php 
                    
                    while ($row = $users->fetch_array()){
                        
                        ?>
                        <tr>
                            <form action="<?php echo $message; ?>" method="" >
                                <td><input type="hidden" name="login" value="<?php echo $row['login']; ?>"><?php echo $row['login']; ?></td>
                                <td><?php echo $row['password']; ?></td> 
                                <td><?php echo $row['DNI']; ?></td>
                                <td><?php echo $row['FechaNacimiento']; ?></td>
                                <td><?php echo $row['telefono']; ?></td>  
                                <td><?php echo $row['email']; ?></td>
                                <td><?php echo $row['nombre']; ?></td>
                                <td><?php echo $row['apellidos']; ?></td>
                                <td><a href="<?php echo $row['fotopersonal']; ?>"><?php echo $row['fotopersonal']; ?></a></td>
                                <td><?php echo $strings[$row['sexo']]; ?></td>  
                                <td>
                                    <button class="showall-action" name="action" value="SHOWCURRENT" type="submit"><img src="../Views/imgs/detail.png" alt="" srcset=""></button>
                                    <button class="showall-action" name="action" value="EDIT" type="submit"><img src="../Views/imgs/edit.png" alt="" srcset=""></button>
                                    <button class="showall-action" name="action" value="DELETE" type="submit"><img src="../Views/imgs/delete.png" alt="" srcset=""></button>
                                </td>
                            </form>
                        </tr>    
                        
                        
                        <?php
                    }
    
                    ?>
                    
                    </table>
        
            </div>
        
            
            <footer>
                <h6><?php echo $strings['Date']; ?>: 11/11/2017</h6>
                <h6><?php echo $strings['Author']; ?>: yn8idg</h6>
            </footer>
            </section>
            
            <script src="../Views/js/main.js"></script>
            <?php include '../Views/js/validaciones.js'  ?>
            <script type="text/javascript" src="../Views/js/tcal.js"></script>
        </body>
        </html>
        
        <?php
    
        }
    }
?>