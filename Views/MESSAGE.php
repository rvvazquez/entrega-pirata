<?php
//Archivo php donde se muestra mensajes de error: yn8idg, Fecha: 11/11/2017
    class MESSAGE {

        function __construct($message,$dir){

            //if(!is_string($user))
            //$user = $user->fetch_array();
            $this->pinta($message,$dir);

        }

    

    function pinta($message,$dir){
        if(!isset($_SESSION['idioma'])){
            $_SESSION['idioma'] = 'SPANISH'; 
        }

        include_once '../Locales/Strings_index.php';

        foreach($stringslang as $lang){
            if($lang == $_SESSION['idioma'])
                include_once '../Locales/Strings_'. $lang .'.php';
        }

        include '../Views/HEADER_View.php';

        new HEADER();
        ?>
                    <aside id="menu">
                        <div class="resp-menu-close" onclick="hide()">
                            <img src="../Views/imgs/cross.png" alt="">
                        </div>
                        <ul>
                            <li onclick="show()" tabindex="1">
                            <?php echo $strings['Option']; ?> 1
                                <ul class="sub" id="pri">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 2
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 3
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 4
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 5
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                        </ul>
                    </aside>
            <section>
                
                
                <div class="form">
                    <h3><?php echo $strings[$message]; ?></h3>
                    <div class="boton-grup">
                        <form action="<?php echo $dir; ?>" method="">
                            <button name="action" value="" type="submit" class="boton-env">
                                <img src="../Views/imgs/ok.png" alt="">
                            </button>
                        </form>
                    </div>
                </div>
                
            <footer>
                <h6><?php echo $strings['Date']; ?>: 11/11/2017</h6>
                <h6><?php echo $strings['Author']; ?>: yn8idg</h6>
            </footer>
            </section>
            <script src="../Views/js/main.js"></script>
            <?php include '../Views/js/validaciones.js'  ?>
            <script type="text/javascript" src="../Views/js/tcal.js"></script>
        </body>
        </html>
        
        <?php
    
        }
    }
?>