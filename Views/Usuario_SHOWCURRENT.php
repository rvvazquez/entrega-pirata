<?php
//Archivo php donde se muestra el apartado Usuario Detallado: yn8idg, Fecha: 11/11/2017
    class USUARIOS_SHOWCURRENT {

        function __construct($user){

            //if(!is_string($user))
            //$user = $user->fetch_array();
            $this->pinta($user);

        }

    

    function pinta($user){
        if(!isset($_SESSION['idioma'])){
            $_SESSION['idioma'] = 'SPANISH'; 
        }

        include_once '../Locales/Strings_index.php';

        foreach($stringslang as $lang){
            if($lang == $_SESSION['idioma'])
                include_once '../Locales/Strings_'. $lang .'.php';
        }

        include '../Views/HEADER_View.php';

        new HEADER();
        ?>
            <aside id="menu">
                <div class="resp-menu-close" onclick="hide()">
                    <img src="../Views/imgs/cross.png" alt="">
                </div>
                <ul>
                    <li onclick="show()" tabindex="1">
                    <?php echo $strings['Option']; ?> 1
                        <ul class="sub" id="pri">
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                        </ul>
                    </li>
                    <li tabindex="1"><?php echo $strings['Option']; ?> 2
                        <ul class="sub">
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                        </ul>
                    </li>
                    <li tabindex="1"><?php echo $strings['Option']; ?> 3
                        <ul class="sub">
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                        </ul>
                    </li>
                    <li tabindex="1"><?php echo $strings['Option']; ?> 4
                        <ul class="sub">
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                        </ul>
                    </li>
                    <li tabindex="1"><?php echo $strings['Option']; ?> 5
                        <ul class="sub">
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                            <li><?php echo $strings['Suboption']; ?> 1</li>
                        </ul>
                    </li>
                </ul>
            </aside>
            <section>
                    
            <div class="form2">
                <h4><?php echo $strings['ShowCurrent']; ?></h4> 
                <h4>Esta es la informacion seleccionada</h4>
                <ul>
                    <li>
                        <h5><?php echo $strings['Login']; ?></h5>
                        <span> <?php echo $user['login']; ?> </span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Password']; ?></h5>
                        <span><?php echo $user['password']; ?></span>
                    </li>
                    <li>
                        <h5><?php echo $strings['DNI']; ?></h5>
                        <span> <?php echo $user['DNI']; ?> </span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Name']; ?></h5>
                        <span><?php echo $user['nombre']; ?></span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Surname']; ?></h5>
                        <span><?php echo $user['apellidos']; ?></span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Email']; ?></h5>
                        <span><?php echo $user['email']; ?></span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Phone']; ?></h5>
                        <span><?php echo $user['telefono']; ?></span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Date birth']; ?></h5>
                        <span><?php echo $user['FechaNacimiento']; ?></span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Sex']; ?></h5>
                        <span><?php echo $strings[$user['sexo']]; ?></span>
                    </li>
                    <li>
                        <h5><?php echo $strings['Photo']; ?></h5>
                        <span><a href="<?php echo $user['fotopersonal']; ?>"><?php echo $user['fotopersonal']; ?></a></span>
                    </li>
                </ul>
                <div class="boton-grup">
                    <form action="../Controllers/USUARIOS_Controller.php" method="">
                        <button name="action" value="" type="submit" class="boton-env">
                            <img src="../Views/imgs/ok.png" alt="">
                        </button>
                    </form>
                    
                </div>
                
            </div>    
            <footer>
                <h6><?php echo $strings['Date']; ?>: 11/11/2017</h6>
                <h6><?php echo $strings['Author']; ?>: yn8idg</h6>
            </footer>
            </section>
            <script src="../Views/js/main.js"></script>
            <?php include '../Views/js/validaciones.js'  ?>
            <script type="text/javascript" src="../Views/js/tcal.js"></script>
        </body>
        </html>
        
        <?php
    
        }
    }
?>