<?php
//Archivo php donde se muestra el apartado Login: yn8idg, Fecha: 11/11/2017
    include_once '../Functions/Authentication.php';

    class Login {

        function __construct(){

            $this->pinta();

        }

    

    function pinta(){

        if(!isset($_SESSION['idioma'])){
            $_SESSION['idioma'] = 'SPANISH'; 
        }

        include_once '../Locales/Strings_index.php';

        foreach($stringslang as $lang){
            if($lang == $_SESSION['idioma'])
                include_once '../Locales/Strings_'. $lang .'.php';
        }

        include '../Views/HEADER_View.php';

        new HEADER();

        ?>
                    <aside id="menu">
                        <div class="resp-menu-close" onclick="hide()">
                            <img src="../Views/imgs/cross.png" alt="">
                        </div>
                        <ul>
                            <li onclick="show()" tabindex="1">
                            <?php echo $strings['Option']; ?> 1
                                <ul class="sub" id="pri">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 2
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 3
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 4
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 5
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                        </ul>
                    </aside>
            <section>
                
                
                <div class="form">
                    <form class="form-basic" method="" action="../Controllers/Login_Controller.php" onsubmit="return comprobarlogin()">
                        <div class="form-group">
                            <label class="form-label" for="login"><?php echo $strings['Login']; ?></label>
                            <input type="text" class="form-control" maxlength="15" size="15" onblur="messagedel(this); comprobarVacio(this); comprobarEspacio(this); comprobarTexto(this,15)" id="login" name="login" placeholder="Your login" tabindex="1">
                        </div>    
                        <div class="form-group">
                            <label class="form-label" for="password1"><?php echo $strings['Password']; ?></label>
                            <input type="password" class="form-control" maxlength="20" size="20" onblur="messagedel(this); comprobarVacio(this); comprobarEspacio(this); comprobarTexto(this,20)" id="password1" name="password" maxlength="5" placeholder="Your password" tabindex="1" >
                        </div>
                        <button name="action" value="" type="submit" class="boton-env">
                            <img src="../Views/imgs/send.png" alt="">
                        </button>
                    </form> 
                    <form action="../Controllers/Registro_Controller.php" class="form-basic">
                        <button name="action" value="NEW" type="submit" class="boton-env">
                            <?php echo $strings['¿Have not you account? Sign up']; ?>
                        </button>
                    </form>     
                </div>
                
            <footer>
                <h6><?php echo $strings['Date']; ?>: 11/11/2017</h6>
                <h6><?php echo $strings['Author']; ?>: yn8idg</h6>
            </footer>
            </section>
            <script src="../Views/js/md5.js"></script>
            <script src="../Views/js/main.js"></script>
            <?php include '../Views/js/validaciones.js'  ?>
            
            <script type="text/javascript" src="../Views/js/tcal.js"></script>
        </body>
        </html>
        
        <?php
    
        }
    }
?>