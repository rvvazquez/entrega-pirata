<?php
//Archivo php donde se muestra el apartado Editar Usuario: yn8idg, Fecha: 11/11/2017
    class USUARIOS_EDIT {

        function __construct($user){

            //if(!is_string($user))
            //$user = $user->fetch_array();
            $this->pinta($user);

        }

    

    function pinta($user){
        if(!isset($_SESSION['idioma'])){
            $_SESSION['idioma'] = 'SPANISH'; 
        }

        include_once '../Locales/Strings_index.php';

        foreach($stringslang as $lang){
            if($lang == $_SESSION['idioma'])
                include_once '../Locales/Strings_'. $lang .'.php';
        }

        include '../Views/HEADER_View.php';

        new HEADER();
        ?>
                    <aside id="menu">
                        <div class="resp-menu-close" onclick="hide()">
                            <img src="../Views/imgs/cross.png" alt="">
                        </div>
                        <ul>
                            <li onclick="show()" tabindex="1">
                            <?php echo $strings['Option']; ?> 1
                                <ul class="sub" id="pri">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 2
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 3
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 4
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 5
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                        </ul>
                    </aside>
            <section>
                    
            <div class="form">
            <h3><?php echo $strings['Edit']; ?></h3>
            <form class="form-basic" enctype="multipart/form-data" method="post" action="../Controllers/USUARIOS_Controller.php" onsubmit="return comprobaredit(1) ">
                
                <div class="form-group">
                    <label class="form-label" for="login"><?php echo $strings['Login']; ?></label>
                    <input readonly type="text" value="<?php echo $user['login']; ?>" class="form-control" maxlength="15" size="15" onblur="messagedel(this); comprobarVacio(this); comprobarEspacio(this); comprobarTexto(this,15)" id="login1" name="login" placeholder="Your login" tabindex="1">
                </div>    
                <div class="form-group">
                    <label class="form-label" for="password"><?php echo $strings['Password']; ?></label>
                    <input type="password" value="<?php echo $user['password']; ?>" class="form-control" maxlength="120" size="120" onblur="messagedel(this); comprobarVacio(this); comprobarEspacio(this); comprobarTexto(this,120)" id="password1" name="password" maxlength="5" placeholder="Your password" tabindex="1" >
                </div> 
                <div class="form-group">
                    <label class="form-label" for="DNI"><?php echo $strings['DNI']; ?></label>
                    <input type="text" value="<?php echo $user['DNI']; ?>" class="form-control" maxlength="9" size="9" onblur="messagedel(this); comprobarVacio(this); comprobarEspacio(this); comprobarDni(this); comprobarTexto(this,20)" id="DNI1" name="DNI" maxlength="5" placeholder="Your DNI" tabindex="1" >
                </div> 
                <div class="form-group">
                    <label class="form-label" for="FechaNacimiento"><?php echo $strings['Date birth']; ?></label>
                    <input type="text" value="<?php echo $user['FechaNacimiento']; ?>" class="tcal" id="date1" name="FechaNacimiento" placeholder="Your birthdate" onblur="messagedel(this); delay(this)" tabindex="1" readonly >
                </div> 
                <div class="form-group">
                    <label class="form-label" for="sexo"><?php echo $strings['Sex']; ?></label>
                    <select name="sexo">
                        <option <?php if ($user['sexo'] == 'hombre' ) echo 'selected ' ; ?> value="hombre"><?php echo $strings['hombre']; ?></option>
                        <option <?php if ($user['sexo'] == 'mujer' ) echo 'selected ' ; ?> value="mujer"><?php echo $strings['mujer']; ?></option>
                    </select>
                </div> 
                <div class="form-group">
                    <label class="form-label" for="email"><?php echo $strings['Email']; ?></label>
                    <input type="text" value="<?php echo $user['email']; ?>" class="form-control" maxlength="60" size="60" onblur="messagedel(this); comprobarVacio(this); comprobarEspacio(this); comprobarEmail(this); comprobarTexto(this,60)" id="email1" name="email" placeholder="Your email" tabindex="1" >
                </div>  
                <div class="form-group">
                    <label class="form-label" for="nombre"><?php echo $strings['Name']; ?></label>
                    <input type="text" value="<?php echo $user['nombre']; ?>" class="form-control" maxlength="25" size="25" onblur="messagedel(this); comprobarVacio(this); comprobarstartEspacio(this); comprobarAlfabetico(this,25)" id="name1" name="nombre" placeholder="Your name" tabindex="1" >
                </div> 
                <div class="form-group">
                    <label class="form-label" for="apellidos"><?php echo $strings['Surname']; ?></label>
                    <input type="text" value="<?php echo $user['apellidos']; ?>" class="form-control" maxlength="50" size="50" onblur="messagedel(this); comprobarVacio(this); comprobarstartEspacio(this); comprobarAlfabetico(this,50)" id="surname1" name="apellidos" placeholder="Your surname" tabindex="1" >
                </div> 
                <div class="form-group">
                    <label class="form-label" for="telefono"><?php echo $strings['Phone']; ?></label>
                    <input type="text" value="<?php echo $user['telefono']; ?>" class="form-control" maxlength="11" size="11" onblur="messagedel(this); comprobarVacio(this); comprobarEspacio(this); comprobarTelf(this)" id="telefono1" name="telefono" placeholder="Your phone" tabindex="1">
                </div> 
                <div class="form-group">
                    <label class="form-label" for="foto"><?php echo $strings['Photo']; ?></label>
                    <input type="file" name="fotopersonal" maxlength="50" size="50" accept="image/gif, image/ jpeg, image/jpg, image/png">
                    <input id="fotoactual" type="hidden" value="<?php echo $user['fotopersonal']; ?>">
                    <?php
                        if($user['fotopersonal']){
                            ?>
                                <span ><?php echo $strings['Your actual photo']; ?>: </span><a href="<?php echo $user['fotopersonal']; ?>"><?php echo $user['fotopersonal']; ?></a>
                            <?php
                        }else{
                            ?>
                                <span ><?php echo $strings['No exist image']; ?></span>
                            <?php
                        }
                    ?>
                </div>  
                    
                <button name="action" value="EDIT" type="submit" class="boton-env">
                    <img src="../Views/imgs/send.png" alt="">
                </button>
            </form>
        </div>

            <footer>
                <h6><?php echo $strings['Date']; ?>: 11/11/2017</h6>
                <h6><?php echo $strings['Author']; ?>: yn8idg</h6>
            </footer>
            </section>
            <script src="../Views/js/md5.js"></script>
            <script src="../Views/js/main.js"></script>
            <?php include '../Views/js/validaciones.js'  ?>
            <script type="text/javascript" src="../Views/js/tcal.js"></script>
        </body>
        </html>
        
        <?php
    
        }
    }
?>