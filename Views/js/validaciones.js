

<script type="text/javascript">
//Archivo javascript con las respectivas validaciones, Autor: yn8idg, Fecha: 11/11/2017


//En todas las funciones se pasa el parametro campo donde es el input que se comprueba
//Con campo.value solicitamos el valor solicitado de ese input


//Importante!! 
//Se puede encontrar funciones similares con un sufijo en sus nombres "console"
//Estas son las mismas funciones que su "hermana" pero adaptada para mostrar la comprobacion por pantalla(console.log())

//Se puede encontrar la funcion message y messagedel(las cuales estan definidas al final del codigo) las cuales interactuan con el DOM 
//para mostrar el error en la interfaz
//////////////////////

//    Funciones solicitadas

//////////////////////


console.log('hhhhhhhhhhh');
//comprobarVacio comprueba que un campo no este vacio

function comprobarVacio(campo){

    //Comprobamos que la longitud no es 0
    if(campo.value.length === 0 || campo.value == ''){
        message("<?php echo $strings['El campo']; ?> " + campo.name + " <?php echo $strings['is empty']; ?>", campo);
        return false;
    }
    return true;
    
    
}


//comprobarTexto comprueba que un campo.value sea menor de un numero de caracteres(size) solicitado

function comprobarTexto(campo, size){
    
    //Comprobamos que la longitud de un value es menor de una dada(size)
    if(campo.value.length > size){
        message("<?php echo $strings['Introduce less of']; ?> " + size + " <?php echo $strings['characters for']; ?> " + campo.name,campo);
        
        return false;
    }
   
    return true;
}

//comprobarAlfabetico comprueba que el valor en el campo solo contenga letras del alfabeto español
//y sus respectivos acentos, dieresis o ñ  y un tamaño indicado

function comprobarAlfabetico(campo, size){

    // Expresion regular que contiene todos los valores que se pueden aceptar
    var letras = /^(([a-zA-ZñÑáéíóúÁÉÍÓÚñ\s])*)$/;

    //Comprueba si el valor introducido contiene algun caracter no permitido
    if(!letras.test(campo.value)){
        message("<?php echo $strings['Introduced characters not allow']; ?>",campo);
        return false;
    }   //Si no se comprueba la longitud de valor introducido
    else if(campo.value > size){
        console.log("<?php echo $strings['Introduce less of']; ?> " + size + " <?php echo $strings['characters']; ?>",campo);
        return false;
    }
    else{
        return true;
    }


}


//comprobarEntero comprueba que un numero entero este entre dos valores solicitados

function comprobarEntero(campo, valormenor, valormayor){
    
    //reg es la expresion regular para comprobar que es un numero entero
    var reg = /([0-9])+/;


    //Si el test de la expresion es correcto se conforma que es un numero sino se exige
    //un valor numerico
    if(!(reg.test(campo.value))){

        message("<?php echo $strings['Introduce numerical value']; ?>",campo);
        return false;

    //Si es un numero se comprueba que esta entre los dos valores dados(valormenor, valormayor)
    }else if(campo.value < valormenor || campo.value > valormayor){
        
        message("<?php echo $strings['Introduce a value between']; ?> " + valormenor + " <?php echo $strings['and']; ?> " + valormayor,campo);
        return false;
    
    }
    return true;

}



//comprobarReal checkea que un campo.value es un numero real y esta entre dos valores dados 
//y solo tenga un numero de decimales permitido

function comprobarReal(campo, numerodecimales, valormenor, valormayor){

    //letr es la expresion regular que comprueba que no haya letra en campo.value
    var letr = /([a-zA-Z])/;
    //reg es la expresion regular que comprueba que es un numero real con los decimales permitidos
    var reg = new RegExp("^[0-9]+(\\.[0-9]{0," + numerodecimales + "})?$");

    //Se comprueba que el valor solo contiene letras
    if(!(letr.test(campo.value))){

        //Se comprueba que el numero es real y no tenga mas decimales de los solicitados
        if(n.test(campo.value)){

            //Se comprueba que el numero real esta entre los dos valores dados
            if(campo.value < valormayor && campo.value > valormenor){
                message("",campo);
            }else{
                message("<?php echo $strings['Introduce a value between']; ?> " + valormenor + " <?php echo $strings['and']; ?> " + valormayor,campo);
                return false;
            }
        }else{
            message("No introduzca mas de " + numerodecimales + " decimales",campo);
            return false;
        }
    }else{
        message("<?php echo $strings['Do not introduce letters']; ?>",campo);
        return false;
    }
    return true;

}



//comprobarDNI compruebe el formato de un  DNI
function comprobarDni(campo){

    //Se comprueba que el valor dado sigue la estructura pedida para DNI
    if(campo.value.length === 9){
        
        //r es la expresion regular para identificar un DNI
        var r= /(^(([0-9]){8}))([A-Z]){1}/;
        
        //Se comprueba si el valor dado tiene formato de DNI
        if(r.test(campo.value)){
            message("",campo);
            return true;
            
        }else{
            message("<?php echo $strings['Incorrect format for DNI']; ?>",campo);
            return false;
        }
    }else{
        message("<?php echo $strings['Incorrect length for DNI']; ?>",campo);
        return false;
    }
}




//comprobarTelf confirma que un telefono nacional o internacional tiene el formato correcto
function comprobarTelf(campo){    
    
    //Expresion regular para identificar un telefono
    var tel = /(^(0034|[^0-9]*)(6|7|8|9)([0-9]){8})/;

    //Se comprueba que el valor dado es un telefono español(nacional o internacional)
    if(tel.test(campo.value)){
        
        return true;
    }
    else{
        message("<?php echo $strings['Incorrect format number']; ?>",campo);
        return false;
    }

}

/////////////////////

//    Funciones extra para los formulario

///////////////////

//comprobarEmail comprueba que el valor del input email tenga un formato de email
function comprobarEmail(campo){
    
    //em es la expresion regular para email
    var em = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    //Se comprueba que el valor dado tiene formato de un email
    if(!(em.test(campo.value))){
        message("<?php echo $strings['Incorrect format email']; ?>",campo);
        return false;
    }else{

    }
    return true;

}

//comprobarEspacio comprueba que no haya valores de espacio (" ") en los valores introducidos
function comprobarEspacio(campo){

    //Expresion regular de espacio (" ")
    var esp = /([ ])/;

    //Comprueba si el valor introducido contiene espacios en blanco
    if(esp.test(campo.value)){
        message("<?php echo $strings['Can not content whitespaces']; ?>",campo);
        return false;
    }

    return true;
}

//comrpobarstartEspacio comprueba que no haya espacios al comienzo del valor introducido
function comprobarstartEspacio(campo){    
        
        //Expresion regular para un espacio al comienzo de un valor
        var esp = /^([ ])/;
        
        //Comprueba si hay espacios al inicia del valor introducido
        if(esp.test(campo.value)){
            message("<?php echo $strings['Can not content whitespaces at beginning']; ?>",campo);
            return false;
            
        }

    return true;
}






///////////////////////////////////////////////////////////////

//Aqui se situan algunas funciones adapatadas para mostrar por consola no implementadas en el HTML



function comprobarAlfabeticoconsole(campo, size){
    
    
    var letras = /^(([a-zA-ZñÑáéíóúÁÉÍÓÚñ\s])*)$/;

    if(!letras.test(campo)){
        console.log("Caracter introducido no permitido");
        return false;
    }
    else if(campo.length > size){
        console.log("Introduzca menos de " + size + " caracteres para ",campo);
        return false;
    }
    else{
        return true;
    }
    
}


function comprobarEnteroconsole(campo, valormenor, valormayor){
    
    var n = /([0-9])+/;

    if(!(n.test(campo))){

        console.log("Introduzca valor numerico");
        return false;

    }else if(campo < valormenor || campo > valormayor){
        
        console.log("Inserte un valor entre " + valormenor + " y " + valormayor);
        return false;
    
    }
    return true;

}

function comprobarRealconsole(campo, numerodecimales, valormenor, valormayor){
    
    var l = /([a-zA-Z])/;

    var n = new RegExp("^[0-9]+(\\.[0-9]{1," + numerodecimales + "})?$");
    
    if(!(l.test(campo))){
        if(n.test(campo)){
            if(campo < valormayor && campo > valormenor){
                return true;
            }else{
                console.log("Introduzca un numero entre " + valormenor + " y " + valormayor);
                return false;
            }
        }else{
            console.log("No introduzca mas de " + numerodecimales + " decimales");
            return false;
        }
    }else{
        console.log("No introduzca letras");
        return false;
    }

}



function comprobarDniconsole(campo){
    
    if(campo.length === 9){

        var r= /(^(([0-9]){8}))([A-Z]){1}/;
        
        if(r.test(campo)){
            
            return true;
        }else{
            console.log("Formato de DNI incorrecto");
            return false;
        }
    }else{
        console.log("Longitud incorrecta para DNI");
        return false;
    }
    return false;

}
console.log("DNI: " + comprobarDniconsole("41212233A"));


function comprobarTelfconsole(campo){

    var tel = /(^(0034|[^0-9]*)(6|7|8|9)([0-9]){8})/;
    
    if(tel.test(campo)){
        
        return true;
    }
    else{
        console.log("Formato del numero incorrecto");
        return false;
    }

}


//comprobar() es la funcion que al hacer el submit comprueba todos los inputs del formulario.
//n difiere entre dos formularios (ADD y EDIT) para que su interacctuacion en el DOM sea correcta 
//Se usaran una variable por cada input en la secuencia del formulario: (login,password, nombre,
//apellidos,email,fecha,grado y titulacion)
//

function comprobar(n){
    
    //Obtenemos los inputs del formulario

    var log = document.getElementById("login"+n);
    var pass = document.getElementById("password"+n);
    var date = document.getElementById("date"+n);
    var em = document.getElementById("email"+n);
    var name = document.getElementById("name"+n);
    var sur = document.getElementById("surname"+n);
    var tele = document.getElementById("telefono"+n);
    var dni = document.getElementById("DNI"+n);
    if(document.getElementById("foto"+n) != null)
        var foto = document.getElementById("foto"+n);
    else{
        console.log('asd');
        var foto = document.getElementById("fotoactual");
    }

    var toret =true;
    //comprobamos los inputs con sus respectivas funciones de comprobaciones 
    if(!(comprobarVacio(log) && comprobarEspacio(log) && comprobarTexto(log,25))){
        toret = false;
        
    }
    if(!(comprobarVacio(pass) && comprobarEspacio(pass) && comprobarTexto(pass,20))){
        toret = false;
        
    }
    if(!(comprobarVacio(name) && comprobarstartEspacio(name) && comprobarAlfabetico(name,20))){
        toret = false;
        
    }
    if(!(comprobarVacio(sur) && comprobarstartEspacio(sur) && comprobarAlfabetico(sur,25))){
        toret = false;
        
    }
    if(!(comprobarVacio(em) && comprobarEspacio(em) && comprobarEmail(em) && comprobarTexto(em,50))){
        toret = false;
        
    }
    if(!(comprobarVacio(date))){
        toret = false;
        
    }
    if(!(comprobarVacio(tele) && comprobarEspacio(tele) && comprobarTelf(tele))){
        toret = false;
        
    }
    if(!(comprobarVacio(dni) && comprobarDni(dni))){
        toret = false;
        
    }
    
    if(!(comprobarVacio(foto))){
        
        toret = false;
        
    }

    if(toret == true){
        encriptar();
    }

   
    return toret;
}


//Editar
function comprobaredit(n){
    
    //Obtenemos los inputs del formulario

    var log = document.getElementById("login"+n);
    var pass = document.getElementById("password"+n);
    var date = document.getElementById("date"+n);
    var em = document.getElementById("email"+n);
    var name = document.getElementById("name"+n);
    var sur = document.getElementById("surname"+n);
    var tele = document.getElementById("telefono"+n);
    var dni = document.getElementById("DNI"+n);
    if(document.getElementById("foto"+n) != null)
        var foto = document.getElementById("foto"+n);
    else{
        console.log('asd');
        var foto = document.getElementById("fotoactual");
    }

    var toret =true;
    //comprobamos los inputs con sus respectivas funciones de comprobaciones 
    if(!(comprobarVacio(log) && comprobarEspacio(log) && comprobarTexto(log,25))){
        toret = false;
        
    }
    if(!(comprobarVacio(pass) && comprobarEspacio(pass) && comprobarTexto(pass,120))){
        toret = false;
        
    }
    if(!(comprobarVacio(name) && comprobarstartEspacio(name) && comprobarAlfabetico(name,20))){
        toret = false;
        
    }
    if(!(comprobarVacio(sur) && comprobarstartEspacio(sur) && comprobarAlfabetico(sur,25))){
        toret = false;
        
    }
    if(!(comprobarVacio(em) && comprobarEspacio(em) && comprobarEmail(em) && comprobarTexto(em,50))){
        toret = false;
        
    }
    if(!(comprobarVacio(date))){
        toret = false;
        
    }
    if(!(comprobarVacio(tele) && comprobarEspacio(tele) && comprobarTelf(tele))){
        toret = false;
        
    }
    if(!(comprobarVacio(dni) && comprobarDni(dni))){
        toret = false;
        
    }
    
    if(!(comprobarVacio(foto))){
        
        toret = false;
        
    }

    if(toret == true){
        encriptar();
    }

   
    return toret;
}

//comprobarbus es lo mismo que comprobar destinado al formulario SEARCH 
//Aqui no se usa la variable n
function comprobarbus(){
    
    var log = document.getElementById("login2");
    var pass = document.getElementById("password1");
    var name = document.getElementById("name2");
    var sur = document.getElementById("surname2");
    var em = document.getElementById("email2");
    var date = document.getElementById("date2");
    var tele = document.getElementById("telefono2");
    var dni = document.getElementById("DNI2");

    var toret =true;

    //Aqui no se comprueba que un input esta vacio
    if(!(comprobarTexto(log) && comprobarEspacio(log) && comprobarEspacio(log))){
        toret = false;
    }
    if(!(comprobarTexto(pass,25) && comprobarEspacio(pass) && comprobarEspacio(pass))){
        toret = false;
    }
    if(!(comprobarAlfabetico(name,20) && comprobarstartEspacio(name))){
        toret = false;
    }
    if(!(comprobarAlfabetico(sur,25) && comprobarstartEspacio(sur))){
        toret = false;
    }
    if(!(comprobarEspacio(em) && comprobarTexto(em,50))){
        toret = false;
    }
    if(!(true)){
        toret = false;
    }
    if(!(comprobarEspacio(tele))){
        toret = false;
    }
    if(!(true)){
        toret = false;  
    }
    

    return toret;
}


function comprobarlogin(){

    var log = document.getElementById("login");
    var pass = document.getElementById("password1");

    var toret =true;

    if(!(comprobarVacio(log) && comprobarEspacio(log) && comprobarTexto(log,25))){
        toret = false;
        console.log(toret);        
    }
    if(!(comprobarVacio(pass) && comprobarEspacio(pass) && comprobarTexto(pass,20))){
        toret = false;
        
    }
    if(toret==true){
        encriptar();
    }
    return toret;

}
/////////
//Funciones extras para interactuar con el DOM


//Utilidad para la funcion siguiente
var UID = {
	_current: 0,
	getNew: function(){
		this._current++;
		return this._current;
	}
};

//Busca los pseudoelementos en DOM dado unos parametros
HTMLElement.prototype.pseudoStyle = function(element,prop,value){
	var _this = this;
	var _sheetId = "pseudoStyles";
	var _head = document.head || document.getElementsByTagName('head')[0];
	var _sheet = document.getElementById(_sheetId) || document.createElement('style');
	_sheet.id = _sheetId;
	var className = "pseudoStyle" + UID.getNew();
	
	_this.className +=  " "+className; 
	
	_sheet.innerHTML += " ."+className+":"+element+"{"+prop+":"+value+"}";
	_head.appendChild(_sheet);
	return this;
};


//Borra la alerta del DOM
function messagedel(campo){
    

        document.getElementById(campo.id).parentElement.pseudoStyle("after","opacity","0");
        document.getElementById(campo.id).parentElement.pseudoStyle("after","content","''");
    
    
}
//Muestra la alerta en el DOM
function message(alerta, campo){
    
    
        if(alerta === ""){        
            document.getElementById(campo.id).parentElement.pseudoStyle("after","opacity","0");
            document.getElementById(campo.id).parentElement.pseudoStyle("after","content","'" + alerta + "'");
        }else{
            document.getElementById(campo.id).parentElement.pseudoStyle("after","opacity","1");
            document.getElementById(campo.id).parentElement.pseudoStyle("after","content","'" + alerta + "'");
        }

   
    
    
}


//Corrige un fallo con el input date dandole unos segundos para ejecutarse 
function delay(campo){
    setTimeout(function() {
       comprobarVacio(campo);
      }, 400);
}

</script>