<?php
//Archivo php donde se muestra el apartado Buscar Usuario: yn8idg, Fecha: 11/11/2017
    class USUARIOS_SEARCH {

        function __construct(){

            $this->pinta();

        }

    

    function pinta(){
        if(!isset($_SESSION['idioma'])){
            $_SESSION['idioma'] = 'SPANISH'; 
        }

        include_once '../Locales/Strings_index.php';

        foreach($stringslang as $lang){
            if($lang == $_SESSION['idioma'])
                include_once '../Locales/Strings_'. $lang .'.php';
        }

        include '../Views/HEADER_View.php';

        new HEADER();
        ?>
                    <aside id="menu">
                        <div class="resp-menu-close" onclick="hide()">
                            <img src="../Views/imgs/cross.png" alt="">
                        </div>
                        <ul>
                            <li onclick="show()" tabindex="1">
                            <?php echo $strings['Option']; ?> 1
                                <ul class="sub" id="pri">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 2
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 3
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 4
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                            <li tabindex="1"><?php echo $strings['Option']; ?> 5
                                <ul class="sub">
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                    <li><?php echo $strings['Suboption']; ?> 1</li>
                                </ul>
                            </li>
                        </ul>
                    </aside>
            <section>
                    
            <div class="form">
            <h3><?php echo $strings['Search']; ?></h3>
           
                <form class="form-basic" method="post" action="../Controllers/USUARIOS_Controller.php" onsubmit="return comprobarbus()">
                    <div class="form-group">
                        <label class="form-label" for="login"><?php echo $strings['Login']; ?></label>
                        <input type="text" class="form-control" maxlength="15" size="15" onblur="messagedel(this); comprobarEspacio(this); comprobarTexto(this,15)" id="login2" name="login"  tabindex="1">
                    </div>    
                    <div class="form-group">
                        <label class="form-label" for="password"><?php echo $strings['Password']; ?></label>
                        <input type="password" class="form-control" maxlength="20" size="20" onblur="messagedel(this); comprobarEspacio(this); comprobarTexto(this,20)" id="password1" name="password" maxlength="5" p tabindex="1" >
                    </div> 
                    <div class="form-group">
                        <label class="form-label" for="DNI"><?php echo $strings['DNI']; ?></label>
                        <input type="text" class="form-control" maxlength="9" size="9" onblur="messagedel(this); comprobarEspacio(this); comprobarTexto(this,9)" id="DNI2" name="DNI" abindex="1" >
                    </div> 
                    <div class="form-group">
                        <label class="form-label" for="FechaNacimiento"><?php echo $strings['Date birth']; ?></label>
                        <input type="text" class="tcal" id="date1" name="FechaNacimiento" onblur="messagedel(this); delay(this)" tabindex="1" readonly >
                    </div> 
                    <div class="form-group">
                        <label class="form-label" for="sexo"><?php echo $strings['Sex']; ?></label>
                        <select name="sexo">
                            <option value="hombre"><?php echo $strings['hombre']; ?></option>
                            <option value="mujer"><?php echo $strings['mujer']; ?></option>
                        </select>
                    </div> 
                    <div class="form-group">
                        <label class="form-label" for="email"><?php echo $strings['Email']; ?></label>
                        <input type="text" class="form-control" maxlength="60" size="60" onblur="messagedel(this); comprobarEspacio(this); comprobarEmail(this); comprobarTexto(this,60)" id="email2" name="email"  tabindex="1" >
                    </div>  
                    <div class="form-group">
                        <label class="form-label" for="nombre"><?php echo $strings['Name']; ?></label>
                        <input type="text" class="form-control" maxlength="30" size="30" onblur="messagedel(this); comprobarstartEspacio(this); comprobarAlfabetico(this,30)" id="name2" name="nombre"  tabindex="1" >
                    </div> 
                    <div class="form-group">
                        <label class="form-label" for="apellidos"><?php echo $strings['Surname']; ?></label>
                        <input type="text" class="form-control" maxlength="50" size="50" onblur="messagedel(this); comprobarstartEspacio(this); comprobarAlfabetico(this,50)" id="surname2" name="apellidos" tabindex="1" >
                    </div> 
                    <div class="form-group">
                        <label class="form-label" for="telefono"><?php echo $strings['Phone']; ?></label>
                        <input type="text" class="form-control" maxlength="11" size="11" onblur="messagedel(this); comprobarEspacio(this); comprobarTexto(this,11); comprobarTelf(this)" id="telefono2" name="telefono" tabindex="1">
                    </div> 
                    <div class="form-group">
                        <label class="form-label" for="foto"><?php echo $strings['Photo']; ?></label>
                        <input type="text" class="form-control" maxlength="50" size="50" onblur="messagedel(this); comprobarVacio(this); comprobarTexto(this,60)" id="foto2" name="fotopersonal" tabindex="1" >
                    </div> 
                    
                    <button name="action" value="SEARCH" type="submit" class="boton-env">
                        <img src="../Views/imgs/send.png" alt="">
                    </button>
                </form>
            </div>
            <footer>
                <h6><?php echo $strings['Date']; ?>: 11/11/2017</h6>
                <h6><?php echo $strings['Author']; ?>: yn8idg</h6>
            </footer>
            </section>
            <script src="../Views/js/md5.js"></script>
            <script src="../Views/js/main.js"></script>
            <?php include '../Views/js/validaciones.js'  ?>
            <script type="text/javascript" src="../Views/js/tcal.js"></script>
        </body>
        </html>
        
        <?php
    
        }
    }
?>