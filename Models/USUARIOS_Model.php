
<?php

//Clase : USUARIOS_Modelo
//Creado el : 11-11-2017
//Creado por: yn8idg
//-------------------------------------------------------

class USUARIOS_Model { //declaración de la clase

	var $login; // declaración del atributo login
	var $DNI; // declaración del atributo DNI
	var $password; // declaración del atributo password
	var $emailuser; // declaración del atributo edad
	var $fechnacuser; // declaración del atributo fecha nacimiento usuario
	var $nombreuser; // declaración del atributo hora nacimiento usuario
	var $apellidouser; // declaración del atributo apellido
	var $telefono; // declaración del atributo telefono
	var $fotopersonalouser; // declaración del atributo foto
	var $sexo; // declaración del atributo sexo
	var $mysqli; // declaración del atributo manejador de la bd

//Constructor de la clase
//

function __construct($login,$DNI,$password,$emailuser,$fechnacuser,$nombreuser,$apellidouser,
	$telefono,$fotopersonalouser,$sexo){
	//asignación de valores de parámetro a los atributos de la clase
	$this->login = $login;
	$this->DNI = $DNI;
	$this->password = $password;
	$this->emailuser = $emailuser;
	$this->nombreuser = $nombreuser;
	$this->apellidouser = $apellidouser;
	$this->telefono = $telefono;
	$this->fotopersonalouser = $fotopersonalouser;
	$this->sexo = $sexo;

	

	//si la fecha de nacimiento viene vacia la asignamos vacia
	if ($fechnacuser == ''){
		$this->fechnacuser = $fechnacuser;
	}
	else{ // si no viene vacia le cambiamos el formato para que se adecue al de la bd
		$this->fechnacuser = date_format(date_create($fechnacuser), 'Y-m-d');
	}

	

	// incluimos la funcion de acceso a la bd
	include '../Models/DB/BdAdmin.php';
	// conectamos con la bd y guardamos el manejador en un atributo de la clase
	$this->mysqli = ConnectDB();

} // fin del constructor



//Metodo ADD()
//Inserta en la tabla  de la bd  los valores
// de los atributos del objeto. Comprueba si la clave/s esta vacia y si 
//existe ya en la tabla
function ADD()
{
    if (($this->login <> '')){ // si el atributo clave de la entidad no esta vacio
		
		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `USUARIO` WHERE (login = '$this->login')";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'It is not possible connect to DB'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO `USUARIO` (`login`,`DNI`, `password`, `FechaNacimiento`,`email`, `nombre`, `apellidos`,
				 `telefono`, `fotopersonal`, `sexo`) 
				VALUES ('".$this->login."','".$this->DNI."', '".$this->password."', '".$this->fechnacuser."','".$this->emailuser."', '".$this->nombreuser."',
				 '".$this->apellidouser."', '".$this->telefono."', '../Files/".$this->fotopersonalouser['name']."', '".$this->sexo."');";
				
				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					if($this->mysqli->error == "Duplicate entry 'alcado94@hotmail.com' for key 'email'")
						return 'Duplicate email';
					else if($this->mysqli->error == "Duplicate entry '45671234A' for key 'DNI'"){
						return 'Duplicate DNI';
					}else{
						return 'Unknowed Error';
					}
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					move_uploaded_file($this->fotopersonalouser['tmp_name'],'../Files/'.$this->fotopersonalouser['name']);	
					return 'Success insert'; //operacion de insertado correcta
				}
				
			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'It is already in DB'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
        return 'Introduce a value'; // introduzca un valor para el usuario
	}
} // fin del metodo ADD

//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

} // fin del metodo destruct

function AllData(){
	$sql = "SELECT * FROM `USUARIO` ";
	if (!($resultado = $this->mysqli->query($sql))){
		return 'It does not exist in DB'; // 
	}
	else{ // si existe se devuelve la tupla resultado
		$result = $resultado;
		return $result;
	}
}

//funcion SEARCH: hace una búsqueda en la tabla con
//los datos proporcionados. Si van vacios devuelve todos
function SEARCH()
{ 	// construimos la sentencia de busqueda con LIKE y los atributos de la entidad
    $sql = "SELECT *
       			from `USUARIO`
				where login LIKE '%".$this->login."%' AND
						DNI LIKE '%".$this->DNI."%' AND
						password LIKE '%".$this->password."%' AND
						email LIKE '%".$this->emailuser."%' AND
						nombre LIKE '%".$this->nombreuser."%' AND
						apellidos LIKE '%".$this->apellidouser."%' AND
						telefono LIKE '%".$this->telefono."%' AND
						FechaNacimiento LIKE '%".$this->fechnacuser."%' AND
						telefono LIKE '%".$this->telefono."%' AND
						sexo LIKE '%".$this->sexo."%' AND
						fotopersonal  LIKE '../Files/%".$this->fotopersonalouser."%'";
    // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $this->mysqli->query($sql))){
		return 'Query Error about DB';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
		return $resultado;
	}
} // fin metodo SEARCH

// funcion DELETE()
// comprueba que exista el valor de clave por el que se va a borrar,si existe se ejecuta el borrado, sino
// se manda un mensaje de que ese valor de clave no existe
function DELETE()
{	// se construye la sentencia sql de busqueda con los atributos de la clase
    $sql = "SELECT * FROM `USUARIO` WHERE (login = '".$this->login."')";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
    // si existe una tupla con ese valor de clave
    if ($result->num_rows == 1)
    {
    	// se construye la sentencia sql de borrado
        $sql = "DELETE FROM `USUARIO` WHERE (login = '".$this->login."')";
        // se ejecuta la query
        $this->mysqli->query($sql);
        // se devuelve el mensaje de borrado correcto
    	return "Correctly delete";
    } // si no existe el login a borrar se devuelve el mensaje de que no existe
    else
        return "It does not exist";
} // fin metodo DELETE

// funcion RellenaDatos()
// Esta función obtiene de la entidad de la bd todos los atributos a partir del valor de la clave que esta
// en el atributo de la clase
function RellenaDatos()
{	// se construye la sentencia de busqueda de la tupla
	$sql = "SELECT * FROM `USUARIO` WHERE login = '".$this->login."'";
    
    // Si la busqueda no da resultados, se devuelve el mensaje de que no existe
    if (!($resultado = $this->mysqli->query($sql))){
		return 'It does not exist in DB'; // 
	}
    else{ // si existe se devuelve la tupla resultado
		$result = $resultado->fetch_array();
		//$result['foto'] = file("'".$result['fotopersonal']."'");
		return $result;
	}
} // fin del metodo RellenaDatos()

// funcion EDIT()
// Se comprueba que la tupla a modificar exista en base al valor de su clave primaria
// si existe se modifica
function EDIT()
{
	// se construye la sentencia de busqueda de la tupla en la bd
    $sql = "SELECT * FROM `USUARIO` WHERE (login = '".$this->login."')";
    // se ejecuta la query
    $result = $this->mysqli->query($sql);
	// si el numero de filas es igual a uno es que lo encuentra
	
    if ($result->num_rows == 1)
	{	// se construye la sentencia de modificacion en base a los atributos de la clase
		
		if($this->fotopersonalouser['name'] == ''){
			
			$sqlfoto = "SELECT fotopersonal FROM `USUARIO` WHERE (login = '".$this->login."')";
			// se ejecuta la query
			$resultfoto = $this->mysqli->query($sql)->fetch_array();

			$this->fotopersonalouser['name'] = substr($resultfoto['fotopersonal'],9);
		}
		$sql = "UPDATE `USUARIO` SET 
					DNI = '".$this->DNI."',
					password = '".$this->password."',
					FechaNacimiento = '".$this->fechnacuser."',
					nombre= '".$this->nombreuser."',
					apellidos = '".$this->apellidouser."',
					telefono = '".$this->telefono."',
					sexo = '".$this->sexo."',
					email = '".$this->emailuser."',
					fotopersonal = '../Files/".$this->fotopersonalouser["name"]."'
				WHERE ( login = '".$this->login."'
				)";
		// si hay un problema con la query se envia un mensaje de error en la modificacion
        if (!($resultado = $this->mysqli->query($sql))){
			if($this->mysqli->error == "Duplicate entry 'alcado94@hotmail.com' for key 'email'")
				return 'Duplicate email';
			else if($this->mysqli->error == "Duplicate entry '45671234A' for key 'DNI'"){
				return 'Duplicate DNI';
			}else{
				return 'Unknowed Error';
			}

		}
		else{ // si no hay problemas con la modificación se indica que se ha modificado
			move_uploaded_file($this->fotopersonalouser['tmp_name'],'../Files/'.$this->fotopersonalouser['name']);	
			return 'Success Modify';
		}
    }
    else // si no se encuentra la tupla se manda el mensaje de que no existe la tupla
    	return 'It does not exist in DB';
} // fin del metodo EDIT

// funcion login: realiza la comprobación de si existe el usuario en la bd y despues si la pass
// es correcta para ese usuario. Si es asi devuelve true, en cualquier otro caso devuelve el 
// error correspondiente
function login(){
	
	$sql = "SELECT *
			FROM `USUARIO`
			WHERE login = '".$this->login."'";
	$resultado = $this->mysqli->query($sql);
	if ($resultado->num_rows == 0){
		return 'User does not exists';
	}
	else{
		$tupla = $resultado->fetch_array();
		if ($tupla['password'] == $this->password){
			return true;
		}
		else{
			return 'Incorrect password for this login';
		}
	}
}//fin metodo login

//
function Register(){

		$sql = "SELECT * from `USUARIO` where login = '".$this->login."'";

		$result = $this->mysqli->query($sql);
		if ($result->num_rows == 1){  // existe el usuario
				return 'User already exists';
			}
		else{
				return true; //no existe el usuario
		}

	}

function registrar(){
	if (($this->login <> '')){ // si el atributo clave de la entidad no esta vacio
		
		// construimos el sql para buscar esa clave en la tabla
        $sql = "SELECT * FROM `USUARIOS` WHERE (login = '$this->login')";

		if (!$result = $this->mysqli->query($sql)){ // si da error la ejecución de la query
			return 'It is not possible connect to DB'; // error en la consulta (no se ha podido conectar con la bd). Devolvemos un mensaje que el controlador manejara
		}
		else { // si la ejecución de la query no da error
			if ($result->num_rows == 0){ // miramos si el resultado de la consulta es vacio (no existe el login)
				//construimos la sentencia sql de inserción en la bd
				$sql = "INSERT INTO `USUARIO` (`login`,`DNI`, `password`, `FechaNacimiento`,`email`, `nombre`, `apellidos`,
				 `telefono`, `fotopersonal`, `sexo`) 
				VALUES ('".$this->login."','".$this->DNI."', '".$this->password."', '".$this->fechnacuser."','".$this->emailuser."', '".$this->nombreuser."',
				 '".$this->apellidouser."', '".$this->telefono."', '../Files/".$this->fotopersonalouser['name']."', '".$this->sexo."');";
				
				if (!$this->mysqli->query($sql)) { // si da error en la ejecución del insert devolvemos mensaje
					if($this->mysqli->error == "Duplicate entry 'alcado94@hotmail.com' for key 'email'")
						return 'Duplicate email';
					else if($this->mysqli->error == "Duplicate entry '45671234A' for key 'DNI'"){
						return 'Duplicate DNI';
					}else{
						return 'Unknowed Error';
					}
				}
				else{ //si no da error en la insercion devolvemos mensaje de exito
					move_uploaded_file($this->fotopersonalouser['tmp_name'],'../Files/'.$this->fotopersonalouser['name']);	
					return 'Success insert'; //operacion de insertado correcta
				}
				
			}
			else // si ya existe ese valor de clave en la tabla devolvemos el mensaje correspondiente
				return 'It is already in DB'; // ya existe
		}
    }
    else{ // si el atributo clave de la bd es vacio solicitamos un valor en un mensaje
        return 'Introduce a value'; // introduzca un valor para el usuario
	}

	
}



}//fin de clase

?> 
